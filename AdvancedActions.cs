﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP.IO;
using System.IO;
using System.Xml.Serialization;

namespace AdvancedActions
{
    //<div>Icons made by <a href="http://www.elegantthemes.com" title="Elegant Themes">Elegant Themes</a>, <a href="http://appzgear.com" title="Appzgear">Appzgear</a>, <a href="http://www.freepik.com" title="Freepik">Freepik</a>, <a href="http://graphicsbay.com" title="GraphicsBay">GraphicsBay</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>         is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
    [KSPAddon(KSPAddon.Startup.EditorAny, false)]
    public class AdvancedActions : MonoBehaviour
    {

        private static GUIStyle windowStyle, labelStyle, settingsButtonStyle;

        
        private static IButton toolbarButton;
        public static bool indicatorIsHidden = true;

        Vessel thisVessel;
        protected Rect windowPos;

        static int rows = 18;

        List<AAE_Part> AApartList = new List<AAE_Part>();

        private static int[] currentAction = new int[rows];

        private static bool dataLoaded = false;
        private List<KSPActionGroup> AllActionGroups = new List<KSPActionGroup>();

        string currentText = "new Preset";
        
        //Presets
        //List<Preset> presets = new List<Preset>();
        Dictionary<string, Preset> presets = new Dictionary<string, Preset>();

        // Configuration
        private static readonly string addonFolder = Path.Combine(Path.Combine(KSPUtil.ApplicationRootPath, "GameData"), "BurrrY");
        private static string versionMajor = "1";
        private static string versionMinor = "0";

        PresetEditWindow pew = null;

        Vector2 scrollPosition = new Vector2();

        public void Awake()
        {
            myPrint("start of AdvancedActionEditor Awake()");

            if (ToolbarManager.ToolbarAvailable)
            {
                toolbarButton = ToolbarManager.Instance.add("AdvancedActionEditor", "aaEditor");
                toolbarButton.TexturePath = "BurrrY/Plugins/ToolbarIcons/screwdriver3";
                toolbarButton.ToolTip = "Show/Hide AdvancedActionEditor";
                toolbarButton.Visibility = new GameScenesVisibility(GameScenes.EDITOR);
                toolbarButton.Visible = true;
                toolbarButton.Enabled = true;
                toolbarButton.OnClick += toolbarButton_OnClick;
                thisVessel = FlightGlobals.ActiveVessel;
            }

            loadPresets();


            //        if (!hasInitializedStyles) initStyles();
            myPrint("end of AdvancedActionEditor Awake()");
        }



#region SaveAndLoad
        private void loadPresets()
        {
            KSP.IO.PluginConfiguration pluginConfig = KSP.IO.PluginConfiguration.CreateForType<AdvancedActions>();
        }

        private void saveData()
        {
            Serialize(presets);
        }

        static public void Serialize(Dictionary<string, Preset> presets)
        {
            List<Preset> list = new List<Preset>();

            foreach (KeyValuePair<string, Preset> KVP in presets)
                list.Add(KVP.Value);

            XmlSerializer serializer = new XmlSerializer(typeof(List<Preset>));
            using (System.IO.TextWriter writer = new StreamWriter(GetAbsoluteConfigurationPath() + "_Presets.xml"))
            {
                serializer.Serialize(writer, list);
            }  
        }

        static public Dictionary<string, Preset> Deserialize()
        {
            string xmlFile = GetAbsoluteConfigurationPath() + "_Presets.xml";

            if(!System.IO.File.Exists(xmlFile))
                return new Dictionary<string, Preset>(); 

            XmlSerializer deserializer = new XmlSerializer(typeof(List<Preset>));
            System.IO.TextReader reader = new StreamReader(xmlFile);
            object obj = deserializer.Deserialize(reader);
            List<Preset> XmlData = (List<Preset>)obj;
            reader.Close();

            Dictionary<string, Preset> dict = new Dictionary<string,Preset>();

            foreach (Preset p in XmlData)
            {
                myPrint(p.ToString() + " L: " + p.data.Count.ToString());
                dict.Add(p.Name, p);
            }

            return dict;
        }

        public static string GetAbsoluteConfigurationPath()
        {
            return Path.Combine
            (
                addonFolder,
                "AdvancedActionPresets_v" + versionMajor + versionMinor
            );
        }



        private void loadData()
        {

            if (dataLoaded)
                return;

            myPrint("LoadData");

            AllActionGroups.Clear();
            presets.Clear();
            AApartList.Clear();
            myPrint("clr");

            foreach (KSPActionGroup ag in Enum.GetValues(typeof(KSPActionGroup)) as KSPActionGroup[])
            {
                AllActionGroups.Add(ag);
            }


            XmlSerializer deserializer = new XmlSerializer(typeof(List<AAE_Part>));
            System.IO.TextReader reader = new StreamReader(GetAbsoluteConfigurationPath() + "_Parts.xml");
            object obj = deserializer.Deserialize(reader);
            AApartList = (List<AAE_Part>)obj;
            reader.Close();
            
            presets = Deserialize();


            /*
             * 
            AAE_Part aaep = new AAE_Part("None");
            AApartList.Add(aaep);

            aaep = new AAE_Part("Cargo Bays", "ToggleAction", new List<string>(new string[] { "CargoBay", "Body.Cargo", "BodyLarge.Cargo" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Solar-Panels","ExtendPanelsAction",new List<string>(new string[] { "Photovoltaic", "Solar" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Liquid-Engines", "OnAction", new List<string>(new string[] { "liquid", "nuclearEngine", "B9.Engine.VS1", "B9.Engine.VA1", "B9.Engine.L2.Atlas", "B9.Structure.HX.HPD1" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Landing-Legs", "OnAction", new List<string>(new string[] { "landingLeg" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Ladders", "ToggleAction", new List<string>(new string[] { "ladder" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Jet-Engines", "OnAction", new List<string>(new string[] { "turboFanEngine", "JetEngine", "B9.Engine.Jet" }));
            AApartList.Add(aaep);

            aaep = new AAE_Part("Multi-Engines", "ModeAction", new List<string>(new string[] { "RAPIER", "Engine.SABRE" }));
            AApartList.Add(aaep);
            */

            dataLoaded = true;
        }

#endregion


        void toolbarButton_OnClick(ClickEvent e)
        {
            if (indicatorIsHidden)
            {

                RenderingManager.AddToPostDrawQueue(3, new Callback(drawGUI));//start the GUI
            }
            else
            {
                RenderingManager.RemoveFromPostDrawQueue(3, new Callback(drawGUI)); //close the GUI
            }

            indicatorIsHidden = !indicatorIsHidden;
        }


        public void Start()
        {
            LoadPrefs();
            dataLoaded = false;
            indicatorIsHidden = true;
            loadData();
            myPrint("end of AdvancedActionEditor Start()");
        }
        public void OnDestroy()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<AAE_Part>));
            using (System.IO.TextWriter writer = new StreamWriter(GetAbsoluteConfigurationPath() + "_Parts.xml"))
            {
                serializer.Serialize(writer, AApartList);
            }  
        }


        public void Update()
        {
           
        }

#region GUI

        private void drawGUI()
        {
            GUI.skin = HighLogic.Skin;
            windowPos = GUILayout.Window(1, windowPos, WindowGUI, "AdvancedActionEditor", GUILayout.MinWidth(300));
           
        }

        private void WindowGUI(int windowID)
        {

            loadData();
            GUIStyle guiBtnStyle = new GUIStyle(GUI.skin.button);
            guiBtnStyle.normal.textColor = guiBtnStyle.focused.textColor = Color.white;
            guiBtnStyle.hover.textColor = guiBtnStyle.active.textColor = Color.yellow;
            guiBtnStyle.onNormal.textColor = guiBtnStyle.onFocused.textColor = guiBtnStyle.onHover.textColor = guiBtnStyle.onActive.textColor = Color.green;
            guiBtnStyle.padding = new RectOffset(8, 8, 8, 8);

            GUIStyle guiLblStyle = new GUIStyle(GUI.skin.box);
            guiLblStyle.padding  = new RectOffset(8, 8, 8, 8);


            GUILayout.BeginVertical();


            currentText = GUILayout.TextField(currentText, GUILayout.ExpandWidth(true)); //you can play with the width of the text box
            if (GUILayout.Button("Add Preset", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                addPreset(currentText);
            }

            GUILayout.Space(10);

            GUILayout.Label("Presets", guiLblStyle, GUILayout.ExpandWidth(true));
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandWidth(true), GUILayout.Height(250));
                foreach (var p in presets)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(p.Value.Name, guiLblStyle, GUILayout.ExpandWidth(true));
                    if (GUILayout.Button("Apply", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                    {
                        applyAction(p.Value.Name);
                    }

                    if (GUILayout.Button("Edit", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                    {
                        editPreset(p.Value.Name);
                    }

                    if (GUILayout.Button("X", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                    {
                        deletePreset(p.Value.Name);
                    }

                    /*
                     * DEBUG-Button
                    if (GUILayout.Button("D", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                    {
                        printPreset(p);
                    }              
                    */
                    GUILayout.EndHorizontal();
                }
            GUILayout.EndScrollView();



            if (GUILayout.Button("Save", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                saveData();
            }
            /*
            if (GUILayout.Button("DBG", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                showPartData();
            }
            */

            GUILayout.EndVertical();


            //DragWindow makes the window draggable. The Rect specifies which part of the window it can by dragged by, and is 
            //clipped to the actual boundary of the window. You can also pass no argument at all and then the window can by
            //dragged by any part of it. Make sure the DragWindow command is AFTER all your other GUI input stuff, or else
            //it may "cover up" your controls and make them stop responding to the mouse.
            GUI.DragWindow(new Rect(0, 0, 10000, 20));

        }

        private void showPartData()
        {
            List<Part> vesselParts = EditorLogic.SortedShipList;
            foreach (Part p in vesselParts)
            {

                PartModuleList Modules = p.Modules;
                foreach (PartModule v in Modules)
                {

                    List<BaseAction> partActions = v.Actions;

                    foreach (BaseAction pm in partActions)
                    {
                        myPrint(p.name + ": '" + v.name + "' : '" + pm.name + "' : '" + pm.guiName);
                    }

                }
            }
        }

        private void printPreset(KeyValuePair<string, Preset> p)
        {
            myPrint(p.Value.ToString());
        }


        void OnGUI()
        {
            if (pew != null)
            {
                if (pew.shouldDestroy())
                    pew = null;
                else
                    pew.OnGUI();
            }
        }



        private void deletePreset(string p)
        {
            presets.Remove(p);
        }

        private void editPreset(string p)
        {
            Preset preset = presets[p];
            pew = new PresetEditWindow(ref preset, AApartList);
        }

        private void applyAction(string p)
        {
            Preset thisPreset = presets[p];

            myPrint("Applying Preset: " + thisPreset.Name);

            foreach (Assignment a in thisPreset.data)
            {
                applyAssignment(a);
            }

        }


        private void addPreset(string currentText)
        {
            myPrint("Adding Preset: " + currentText);
            Preset p = new Preset();
            p.Name = currentText;

            if (!presets.ContainsKey(currentText))
            presets.Add(p.Name, p);
        }

#endregion


        private void applyAssignment(Assignment a)
        {
            string selectedType = a.PartType.Name;


            if (selectedType == "None")
                return;

            List<string> filter = a.PartType.partIdentificator;


            List<Part> vesselParts = EditorLogic.SortedShipList;
            bool partFound = false;

            foreach (Part p in vesselParts)
            {
                IEnumerable<string> filtered = filter.Where(str => (str.IndexOf(p.name, StringComparison.OrdinalIgnoreCase) >= 0) || p.name.IndexOf(str, StringComparison.OrdinalIgnoreCase) >= 0);
                bool containsIt = false;

                foreach (string s in filtered)
                    containsIt = true;

                if (containsIt)
                {
                    partFound = true;
                    PartModuleList Modules = p.Modules;
                    foreach (PartModule v in Modules)
                    {
                        
                        List<BaseAction> partActions = v.Actions;

                        string actionName = a.PartType.actionIdentificator;

                        if (selectedType == "Fuel Tanks" || selectedType.Contains("Fuel"))
                        {
                            myPrint("Fuel Tanks");
                            foreach (BaseAction pm in partActions)
                            {
                                myPrint(pm.name);
                            }
                        }
                        else
                        {
                            myPrint(selectedType);
                        }


                        bool actionFound = false;
                        foreach (BaseAction pm in partActions)
                        {
                            if (actionName.Equals(pm.name,StringComparison.InvariantCultureIgnoreCase))
                            {
                                pm.actionGroup = a.AG;
                                actionFound = true;
                            }
                            else if (actionName == "N.A." || actionName == null || !actionFound)
                            {
                                myPrint(p.name + ": " + v.name + ":" + pm.name + ":" + pm.guiName);
                                pm.actionGroup = a.AG;
                            }
                        }    
                    }
                }               
            }

            if (!partFound)
            {
                myPrint("NotFound:" + selectedType);
            }
        }

        static private void myPrint(string p)
        {
            print(p);
        }




        #region Preferences

        public static void LoadPrefs()
        {
            
            //myPrint("End Load Prefs");
        } 
        #endregion

        #region Resources

        private void initStyles()
        {
            Color lightGrey = new Color(.8f, .8f, .85f);

            windowStyle = new GUIStyle(HighLogic.Skin.window);
            windowStyle.stretchWidth = true;
            windowStyle.stretchHeight = true;

            labelStyle = new GUIStyle(HighLogic.Skin.label);
            labelStyle.stretchWidth = true;
            labelStyle.stretchHeight = true;
            labelStyle.normal.textColor = lightGrey;

            settingsButtonStyle = new GUIStyle(HighLogic.Skin.button);
            settingsButtonStyle.padding = new RectOffset(1, 1, 1, 1);
            settingsButtonStyle.stretchHeight = true;
            settingsButtonStyle.stretchWidth = false;
            settingsButtonStyle.fontSize = 11;
            settingsButtonStyle.normal.textColor = lightGrey;

        }

        private static void loadTextures()
        {


        }
        #endregion

    }
}
