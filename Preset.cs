﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdvancedActions
{
    public class Preset : IEquatable<Preset>
    {
        //ActionGroup, PartGroup
        public List<Assignment> data = new List<Assignment>();
        public string Name;

        public Preset()
        {
            
        }
        public void reInit()
        {
            data.Clear();
            foreach (KSPActionGroup ag in Enum.GetValues(typeof(KSPActionGroup)) as KSPActionGroup[])
            {
                data.Add(new Assignment(KSPActionGroup.None, new AAE_Part("None")));
            }
        }

        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            res.Append("Name: " + Name + Environment.NewLine);

            foreach (Assignment s in data)
            {
                res.Append(s.ToString() + Environment.NewLine );
            }

            return res.ToString();
        }

        public bool Equals(Preset other)
        {
            return this.Name == other.Name;
        }
    }

    public class Assignment
    {
        public KSPActionGroup AG;
        public AAE_Part PartType;

        public Assignment() { }

        public Assignment(KSPActionGroup a, AAE_Part p)
        {
            this.AG = a;
            this.PartType = p;
        }

        public override string ToString()
        {
            return AG.ToString() + ": " + PartType.Name;
        }

    }
}

