# README #

This is  Plugin to create Presets for Actiongroups.
You can determine, which PartGroup should be assign to a specific Action-Group, and apply the preset with a simple Click.
It works now with Stock & B9-Parts.

At this point, following Actions are supported:

* Toggle Cargo-Bay-Doors
* Toggle Solar-Panels
* Toggle LiquidFuel-Engines
* Toggle Landinglegs
* Toggle Jet-Engines
* Toggle Ladders
* Switch Mode on Multi-Mode Engines

Feel free to share your Opinion!

-----
Technically i've got a list with Pattern for Partnames and Actionnames, which the plugin searches when you hit apply. Based on this it adds the desired Actions to the Group.

Extending the supported parts-list is more or less easy, the plugin reads a XML-File on startup, which contains the Data. There you could add your Parts, but you need the internal Names, not the one which are Displayed. It also requires the Toolbar-Plugin.