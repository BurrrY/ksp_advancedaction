﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdvancedActions
{
    public class AAE_Part
    {
        public string Name;
        public List<string> partIdentificator = new List<string>();
        public string actionIdentificator;

        public AAE_Part() { }
        public AAE_Part(string p1, string p2, List<string> list)
        {
            // TODO: Complete member initialization
            this.Name = p1;
            this.actionIdentificator = p2;
            this.partIdentificator = list;
        }

        public AAE_Part(string p)
        {
            // TODO: Complete member initialization
            this.Name = p;
        }

        public override string ToString()
        {
            StringBuilder r =  new StringBuilder("AAEPart: '" + Name + "'");
            r.Append("\r\nAI:" + actionIdentificator);
            foreach(var s in partIdentificator)
                r.Append("\r\nPI:" + s);

            return r.ToString();
        }
    }
}
