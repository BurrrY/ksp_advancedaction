﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP.IO;
using System.IO;
using System.Xml.Serialization;

namespace AdvancedActions
{
    public class PresetEditWindow
    {
        private Preset p;
        private const int rows = 18;
        public Rect windowRect = new Rect(448, 128, 400, 730);

        private List<KSPActionGroup> AllActionGroups = new List<KSPActionGroup>();


        private int[] currentSelectedPartTypeIdx = new int[rows];
        private List<AAE_Part> currentSelectedPartType = new List<AAE_Part>();
        private List<string> partTypes = new List<string>(rows);
        private List<AAE_Part> AApartList;


        public bool dataLoaded = false;
        public bool closed = false;

        public PresetEditWindow(ref Preset pre)
        {
            p = pre;
        }

        public PresetEditWindow(ref Preset preset, List<AAE_Part> AApartList)
        {
            // TODO: Complete member initialization
            this.p = preset;
            if (preset.data.Count != 18)
                p.reInit();


            this.AApartList = AApartList;
        }

        public virtual void DoWindow(int window)
        {
            GUI.DragWindow(new Rect(0, 0, 10000, 20));
            GUIStyle guiBtnStyle = new GUIStyle(GUI.skin.button);
            guiBtnStyle.normal.textColor = guiBtnStyle.focused.textColor = Color.white;
            guiBtnStyle.hover.textColor = guiBtnStyle.active.textColor = Color.yellow;
            guiBtnStyle.onNormal.textColor = guiBtnStyle.onFocused.textColor = guiBtnStyle.onHover.textColor = guiBtnStyle.onActive.textColor = Color.green;
            guiBtnStyle.padding = new RectOffset(8, 8, 8, 8);

            GUIStyle guiLblStyle = new GUIStyle(GUI.skin.box);
            guiLblStyle.padding = new RectOffset(8, 8, 8, 8);


            GUILayout.Label("Editing Layout: " + p.Name, GUILayout.ExpandWidth(true));


            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            foreach (KSPActionGroup ag in AllActionGroups)
            {
                GUILayout.Label(ag.ToString(), guiLblStyle, GUILayout.ExpandWidth(true));
            }
            GUILayout.EndVertical();


            GUILayout.BeginVertical();
            for (int i = 0; i < rows; ++i)
            {
                GUILayout.Label(currentSelectedPartType[i].Name, guiLblStyle, GUILayout.ExpandWidth(true));
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            for (var i = 0; i < rows; ++i)
            {
                if (GUILayout.Button("<<", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                {
                    prevAction(i);
                }
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            for (var i = 0; i < rows; ++i)
            {
                if (GUILayout.Button(">>", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
                {
                    nextAction(i);
                }
            }
            GUILayout.EndVertical();



            GUILayout.EndHorizontal();



            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save & Close", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                close(true);
            }
            if (GUILayout.Button("Close", guiBtnStyle, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                close(false);
            }
            GUILayout.EndHorizontal();
        }

        private void close(bool save)
        {
            if (save)
                savePreset();


            this.closed = true;
        }

        private void savePreset()
        {
            p.data.Clear();

            for (var i = 0; i < rows; ++i)
            {
                p.data.Add(new Assignment(AllActionGroups[i], currentSelectedPartType[i]));
            }
        }

        private void loadData()
        {
            if (dataLoaded)
                return;


            MonoBehaviour.print("dataLoaded");


            AllActionGroups.Clear();
            currentSelectedPartType.Clear();

            foreach (KSPActionGroup ag in Enum.GetValues(typeof(KSPActionGroup)) as KSPActionGroup[])
            {
                AllActionGroups.Add(ag);
            }

            for (var i = 0; i < rows; ++i)
            {
                currentSelectedPartType.Add(p.data[i].PartType);
                currentSelectedPartTypeIdx[i] = AApartList.IndexOf(p.data[i].PartType);
            }

            dataLoaded = true;
        }

        private void nextAction(int i)
        {
            int idx = currentSelectedPartTypeIdx[i];
            ++idx;

            if (idx >= AApartList.Count)
                idx = 0;

            currentSelectedPartType[i] = AApartList[idx];
            currentSelectedPartTypeIdx[i] = idx;
        }

        private void prevAction(int i)
        {
            int idx = currentSelectedPartTypeIdx[i];
            --idx;

            if (idx < 0)
                idx = AApartList.Count-1;

            currentSelectedPartType[i] = AApartList[idx];
            currentSelectedPartTypeIdx[i] = idx;
        }


        public void OnGUI()
        {
            loadData();
            
            windowRect = GUI.Window(1337 , windowRect, DoWindow, "AdvancedActions Preset Editor");
            windowRect = ClampRect(windowRect, new Rect(0, 0, Screen.width, Screen.height));

        }

        //FROm Advaced FlyByWire
        public static Rect ClampRect(Rect rect, Rect container)
        {
            Rect item = rect;

            if (item.x < container.x)
            {
                item.x = container.x;
            }
            else if (item.x + item.width >= container.x + container.width)
            {
                item.x = container.x + container.width - item.width;
            }

            if (item.y < container.y)
            {
                item.y = container.y;
            }
            else if (item.y + item.height > container.y + container.height)
            {
                item.y = container.y + container.height - item.height;
            }

            return item;
        }

        internal bool shouldDestroy()
        {
            return closed;
        }

    }
}
